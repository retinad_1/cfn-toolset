import json

from lib.logger import log
import sub_commands.prepare_lambda as prepare_lambda
import sub_commands.prepare_resource as prepare_resource

def process(template_path, components):
    with open(template_path) as root_template_file:
        root_template = json.loads(root_template_file.read())

        for component in components:
            cfn_config = _get_config(component['dir'] + component['cfn_config_path'])
            template = None

            if cfn_config['template_type'] == 'resource':
                template = prepare_resource.process(
                    cfn_config['template_path'],
                    component['dir']
                )
            elif cfn_config['template_type'] == 'lambda':
                template = prepare_lambda.process(
                    cfn_config['paths_to_include'],
                    cfn_config['template_path'],
                    cfn_config['bucket_name'],
                    cfn_config['before_deploy_scripts'],
                    component['dir']
                )

            if template:
                root_template['Resources'] = dict(root_template['Resources'], **template['Resources'])

    return root_template

def _get_config(config_file_path):
    try:
        if config_file_path: log('Using config from %s' % (config_file_path))
    except FileNotFoundError as error:
        if config_file_path: log('No such file or directory %s. Will try to use the CLI arguments.' % (config_file_path))
        raise error

    with open(config_file_path, 'r') as config_file:
        return json.loads(config_file.read())
