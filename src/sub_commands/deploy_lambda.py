import lib.cloudformation as cloudformation
import sub_commands.prepare_lambda as prepare_lambda

def process(template_path, stack_name, paths_to_include, bucket_name, before_deploy_scripts):
    packaged_lambda_template = prepare_lambda.process(paths_to_include, template_path, bucket_name, before_deploy_scripts)

    existing_stack_template = cloudformation.get_template(stack_name)
    full_updated_template = existing_stack_template
    full_updated_template['Resources'] = dict(existing_stack_template['Resources'], **packaged_lambda_template['Resources'])

    cloudformation.deploy(stack_name, full_updated_template)
