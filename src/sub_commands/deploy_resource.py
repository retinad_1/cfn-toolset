import lib.cloudformation as cloudformation
import sub_commands.prepare_resource as prepare_resource

def process(template_path, stack_name):
    json_template = prepare_resource.process(template_path)
    cloudformation.deploy(stack_name, json_template)
