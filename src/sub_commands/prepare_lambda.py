import os
import zipfile

import lib.cloudformation as cloudformation
from lib.shell_command_runner import run
from lib.logger import log

ARCHIVE_NAME = 'package.zip';
ARCHIVE_FULL_PATH = './iac/' + ARCHIVE_NAME;

def process(paths_to_include, template_path, bucket_name, before_deploy_scripts, root_dir):
    _run_scripts_before_package(before_deploy_scripts, root_dir)
    _create_package(paths_to_include, root_dir)

    cfm_template = cloudformation.package(root_dir + '/' + template_path, bucket_name)

    os.remove(root_dir + '/' + ARCHIVE_FULL_PATH)

    return cfm_template

def _create_package(paths_to_include, root_dir):
    log('Creating ' + root_dir + '/' + ARCHIVE_FULL_PATH)
    iac_package = zipfile.ZipFile(root_dir + '/' + ARCHIVE_FULL_PATH, mode='w')

    try:
        paths_to_include = paths_to_include or ['.']

        log('Preparing to include files from: [%s]' % ', '.join(map(str, paths_to_include)))

        for path_to_include in paths_to_include:
            full_path = root_dir + '/' + path_to_include

            if os.path.isdir(full_path):
                files_to_include = _get_all_file_paths(full_path)
                log('Adding content of directory %r recursively to %r (contains %r files)' % (full_path, ARCHIVE_NAME, len(files_to_include)))

                for file_to_include in files_to_include:
                    _add_file_to_archive(file_to_include, iac_package)
            else:
                log('Adding %r to %r' % (full_path, ARCHIVE_NAME))
                _add_file_to_archive(full_path, iac_package)
    finally:
        iac_package.close()

def _add_file_to_archive(file_to_include, archive):
    try:
        archive.write(file_to_include)
    except FileNotFoundError as error:
        print(error)

def _get_all_file_paths(directory):
    file_paths = []

    for root, directories, files in os.walk(directory):
        for filename in files:
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
    return file_paths

def _run_scripts_before_package(before_deploy_scripts, root_dir):
    for before_deploy_script in before_deploy_scripts:
        run(before_deploy_script, root_dir)
