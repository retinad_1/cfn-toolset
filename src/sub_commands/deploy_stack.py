import lib.cloudformation as cloudformation
from lib.logger import log
import sub_commands.prepare_stack as prepare_stack

def process(template_path, components, stack_name):
    template = prepare_stack.process(template_path, components)

    cloudformation.deploy(stack_name, template)
