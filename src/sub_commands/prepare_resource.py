import json

def process(template_path, root_dir):
    with open(root_dir + '/' + template_path, 'r') as template_file:
        return json.loads(template_file.read())
