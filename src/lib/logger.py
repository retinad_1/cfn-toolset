import json

from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import TerminalFormatter

import config.general_config as general_config

def log(object_to_log, prefix=None):
    if prefix:
        print_if_verbose(prefix)

    if isinstance(object_to_log, dict):
        string = format_dict_as_json(object_to_log)
    elif isinstance(object_to_log, str):
        try:
            jsonized_string = json.loads(object_to_log);
            string = format_dict_as_json(jsonized_string)
        except ValueError as error:
            string = object_to_log
    else:
        string = object_to_log

    print_if_verbose(string)

def format_dict_as_json(dict):
    string_to_format = json.dumps(dict, indent=4, sort_keys=True)
    return highlight(string_to_format, JsonLexer(), TerminalFormatter())

def print_if_verbose(string):
    if general_config.VERBOSE:
        print(string)
