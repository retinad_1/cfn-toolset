import subprocess

def run(command, root_dir='.'):
    return subprocess.check_output(command, shell=True, cwd=root_dir).decode('ascii')
