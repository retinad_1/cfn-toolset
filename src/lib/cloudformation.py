import boto3
from botocore.exceptions import ClientError
import json
import os
import random
import subprocess

from lib.logger import log
from lib.shell_command_runner import run

def get_template(stack_name):
    try:
        existing_stack_template = boto3_client().get_template(StackName=stack_name)['TemplateBody']
    except ClientError as error:
        # If the stack is not created yet
        existing_stack_template = {}

    log(existing_stack_template, 'Existing template for stack %s:' % stack_name)

    return existing_stack_template

def package(template_path, bucket_name):
    with open(template_path, 'r') as template_file:
        log(template_file.read(), 'Using input template %s:' % (template_path))

    try:
        command_result = run('aws cloudformation package \
          --template-file %s \
          --s3-bucket %s \
          --use-json' % (template_path, bucket_name))

        packaged_template = json.loads(command_result)
        log(packaged_template, 'Template file generated:')
    except subprocess.CalledProcessError as error:
        log("'aws cloudformation package' failed with: %s" % (error))
        raise error
    except json.decoder.JSONDecodeError as error:
        log("'aws cloudformation package' failed with: %s" % (command_result))
        raise error

    return packaged_template

def deploy(stack_name, json_template):
    log(json_template, 'Deploying template:')

    temporary_file_path = 'tmp_%d' % (random.randint(0, 1000000))

    with open(temporary_file_path, 'w+') as temporary_file:
        temporary_file.write(json.dumps(json_template))
        log('Deploying to %s' % (stack_name))

    try:
        deployment_result = run('aws cloudformation deploy \
          --template-file %s \
          --stack-name %s \
          --capabilities CAPABILITY_IAM' % (temporary_file_path, stack_name))

        log(deployment_result)
    except subprocess.CalledProcessError as error:
        deployment_result = error

    os.remove(temporary_file_path)

    return deployment_result

def boto3_client():
    return boto3.client('cloudformation')
