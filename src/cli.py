#!/usr/local/bin/python3

import click
import json

import config.general_config as general_config
from sub_commands import deploy_lambda as sub_commands_deploy_lambda
from sub_commands import deploy_resource as sub_commands_deploy_resource
from sub_commands import deploy_stack as sub_commands_deploy_stack
from lib.logger import log

DEFAULT_CONFIG_FILE_PATH = './iac/cfn-config.json'

@click.group()
def cli():
    pass

@click.command('deploy')
@click.option('--config-file-path', help='Path to the config file. Command line options will override the config files values.')
@click.option('--verbose/--silent', default=False, help='Enable verbosity.')
def deploy(config_file_path, verbose):
    _set_verbosity(verbose)
    cfn_config = _get_config(config_file_path)

    if cfn_config['template_type'] == 'resource':
        _deploy_resource(cfn_config['template_path'], cfn_config['stack_name'])
    elif cfn_config['template_type'] == 'lambda':
        _deploy_lambda(
            cfn_config['template_path'],
            cfn_config['stack_name'],
            cfn_config['paths_to_include'],
            cfn_config['bucket_name'],
            cfn_config['before_deploy_scripts']
        )
    elif cfn_config['template_type'] == 'stack':
        sub_commands_deploy_stack.process(
            cfn_config['template_path'],
            cfn_config['components'],
            cfn_config['stack_name']
        )

@click.command('deploy-resource')
@click.option('--config-file-path', help='Path to the config file. Command line options will override the config files values.')
@click.option('--stack-name', help='Name of the stack to create / update')
@click.option('--template-path', help='Path to the template file.')
@click.option('--verbose/--silent', default=False, help='Enable verbosity.')
def deploy_resource(config_file_path, stack_name, template_path, verbose):
    _set_verbosity(verbose)
    cfn_config = _get_config(config_file_path)

    if cfn_config:
        stack_name = stack_name or cfn_config['stack_name']
        template_path = template_path or cfn_config['template_path']

    _deploy_resource(template_path, stack_name)

@click.command('deploy-lambda')
@click.option('--before-deploy-scripts', help='Scripts to run before deployment (Basically build scripts)')
@click.option('--bucket-name', help='S3 bucket to use as a destination for the packaged source code.')
@click.option('--config-file-path', help='Path to the config file. Command line options will override the config files values.')
@click.option('--paths-to-include', help='List of path to include when packaging, as comma separated names.')
@click.option('--stack-name', help='Name of the stack to create / update')
@click.option('--template-path', help='Path to the template file.')
@click.option('--verbose/--silent', default=False, help='Enable verbosity.')
def deploy_lambda(before_deploy_scripts, bucket_name, config_file_path, paths_to_include, stack_name, template_path, verbose):
    _set_verbosity(verbose)
    cfn_config = _get_config(config_file_path)

    if cfn_config:
        template_path = template_path or cfn_config['template_path']
        stack_name = stack_name or cfn_config['stack_name']
        paths_to_include = paths_to_include or cfn_config['paths_to_include']
        bucket_name = bucket_name or cfn_config['bucket_name']
        before_deploy_scripts = before_deploy_scripts or cfn_config['before_deploy_scripts']

    _deploy_lambda(template_path, stack_name, paths_to_include, bucket_name, before_deploy_scripts)


def _set_verbosity(verbose):
    general_config.VERBOSE = verbose

def _get_config(config_file_path):
    try:
        config_file_path = config_file_path or DEFAULT_CONFIG_FILE_PATH

        if config_file_path: log('Using config from %s' % (config_file_path))
    except FileNotFoundError:
        if config_file_path: log('No such file or directory %s. Will try to use the CLI arguments.' % (config_file_path))

    with open(config_file_path, 'r') as config_file:
        return json.loads(config_file.read())

def _deploy_lambda(template_path, stack_name, paths_to_include, bucket_name, before_deploy_scripts):
    log('Using parameters:')
    log('  template_path: %s' % (template_path))
    log('  stack_name: %s' % (stack_name))
    log('  paths_to_include: %s' % (paths_to_include))
    log('  bucket_name: %s' % (bucket_name))
    log('  before_deploy_scripts: %s' % (before_deploy_scripts))

    sub_commands_deploy_lambda.process(template_path, stack_name, paths_to_include, bucket_name, before_deploy_scripts)

def _deploy_resource(template_path, stack_name):
    log('Using parameters:')
    log('  template_path: %s' % (template_path))
    log('  stack_name: %s' % (stack_name))

    sub_commands_deploy_resource.process(template_path, stack_name)


cli.add_command(deploy)
cli.add_command(deploy_lambda)
cli.add_command(deploy_resource)

if __name__ == '__main__':
    cli()
